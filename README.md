# Description
Projet en python - flask affichant et permettant des traitements sur un base de données
Les membres sont :
* Manon CHAMPDAVEINE
* Domitille BERTIN
* Antoine LENOIR

# Installation et lancement
```{r, engine='bash', count_lines}
virtualenv ../venv
source ../venv/bin/activate
pip install -r requ.txt
python manage.py loaddb albums.yml

python manage.py runserver
```
# Fonctionnealitées principales
* page principale avec playlist youtube aléatoire dans /
* affichage des artistes disponibles par lettre dans /artistes
* affichage des artistes commençants par une lettre dans /artistes/<lettre>, avec pagination (/artistes/<lettre>/<page>)
* affchage d'un artiste en détail dans /artistes/artiste/<artiste>
* recherche multi critere dans /search et affichage des resultats paginés dans /result/<type>/<champ>/<page>
* informations concernant le site dans /about

Les images sont hébergée sur un serveur distant (http://91.121.156.109/img_rockdb/)
Elles sont disponibles ici : https://mega.nz/#!DwIxDBAT!KwLqJqSHm4swvyHGAGZLvOoKdfTChhZE9L3xdFclGyo