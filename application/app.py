#! /usr/bin/env python3
import os.path

def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))

from flask import Flask
app = Flask(__name__)
app.debug = True

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../sqlite.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from flask.ext.script import Manager
manager = Manager(app)

app.config['SECRET_KEY']="a629b304-0fd0-47c6-8564-065c142262a2"

from flask.ext.bootstrap import Bootstrap
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)
