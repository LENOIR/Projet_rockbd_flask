from .app import *
from .models import *
from .config import *
import yaml, random, pprint, os
from flask import render_template, url_for, redirect, request





@app.route("/")
def home():

	rng = random.randint(0, len(playlists)-1)
	iframe = playlists[rng][0]
	titre_playliste = playlists[rng][1]

	print((rng, iframe, titre_playliste))

	return render_template("home.html",title="Home", tab="Accueil", titre_playliste=titre_playliste, iframe=iframe)


@app.route("/artistes")
def artistes():
	"""
	Affiche une liste pour choisir
	"""
	alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	albums=[]
	for lettre in alphabet:
		album = getAlbum(lettre)
			#return (img, title, artist)
		albums.append(album)

	return render_template("artistes.html", title="Artists", tab="Artistes", albums=albums, alphabet=alphabet)


@app.route("/artistes/<lettre>")
@app.route("/artistes/<lettre>/<int:page>")
def artistesLettre(lettre, page=0):
	"""
	Retourne tout les artistes commencant par une lettre avec pagination
	"""

	page = int(page)
	
	if len(lettre) > 1:
		return "don't edit link manually :) <script>window.location.replace('/artistes/"+lettre[0]+"');</script>"

	artistes = getArtistesByString(lettre)

	page_max = len(artistes)/10

	artistes = artistes[page*10:(page+1)*10]

	artistesGenre = []
	for artiste in artistes:
		artistesGenre.append( (artiste, ", ".join(getGenreArtist(artiste))) )

	return render_template("detailArtistes.html", title="Artistes commencants par "+lettre, tab="Artistes", lettre=lettre, artistes=artistesGenre, page=page, ITpage=list(range(int(page_max)+1)) )


@app.route("/artistes/artiste/<artiste>")
def detailArtiste(artiste):
	return render_template("afficherArtiste.html", title="artiste "+artiste, tab="Artistes", artiste=artiste, albums=getAlbums(artiste) )


@app.route("/search")
def search():
	return render_template("recherche.html", title="Recherche", tab="Recherche")


@app.route('/result', methods=['GET', 'POST'])
@app.route('/result/<int:page>')
@app.route('/result/<int:_type>/<champ>/<int:page>')
def result(_type="", champ="",page=0):

	resultat=[]
	if request.method=='POST':
		resultat = recherche(request.form['champ'], int(request.form['type']), page_size=9, page=page)
	else:
		resultat = recherche(champ, int(_type), page_size=9, page=page)
	_len = len(resultat)
	resultat = resultat[:100]

	if champ=="":
		champ = request.form['champ']
	if _type=="":
		_type = request.form['type']

	cat=""
	if int(_type) == 0:
		cat = "titre"
	if int(_type) == 1:
		cat = "artiste"
	if int(_type) == 2:
		cat = "genre"
	if int(_type) == 3:
		cat = "annee"

	return render_template("resRecherche.html",title="Resultat", tab="Recherche", recherche=champ, cat=cat, nb_res=_len, resultat=resultat, champ=champ, type=_type, page=page)


@app.route("/about") 
def about():
	return render_template("about.html",title="A propos", tab="propos")