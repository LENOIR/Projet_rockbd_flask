from .app import db

#warning, le code qui va suivre est en frenglish, merci de votre comprehension

class Album(db.Model):
	Id     = db.Column(db.Integer, primary_key = True)
	artist = db.Column(db.String(100))
	img    = db.Column(db.String(200))
	parent = db.Column(db.String(100))
	title  = db.Column(db.String(150))
	releaseYear = db.Column(db.Integer)
	def __repr__(self):
		return "<Album %s>" % (self.title)

class Genre(db.Model):
	id      = db.Column(db.Integer, primary_key = True)
	entryId = db.Column(db.Integer)
	genre   = db.Column(db.String(80))
	def __repr__(self):
		return "<Genre %s>" % (self.genre)

def transcode(chaine):
	#MERCI FLO JTM
	import unicodedata
	return chaine.encode('ascii','ignore').decode()

def getAlbums(firstChar):
	"""
	return (img, title, artist)
	"""
	from sqlalchemy import desc
	albums = db.session.query(Album.artist, Album.img, Album.title).distinct(Album.artist).order_by(desc(Album.artist)).all()
	res=[]
	for album in albums:
		if album[0] != None and album[0][0].lower() == firstChar.lower():
			import unicodedata
			artist = album[0].encode('ascii','ignore').decode()
			title = album[2].encode('ascii','ignore').decode()
			img = str(album[1]).encode('ascii','ignore').decode()
			res.append((img, title, artist))
	return res

def getAlbum(firstChar):
	"""
	retourne le premier album qui commence par la lettre en paramettre
	return (img, title, artist)
	"""
	from sqlalchemy import desc
	albums = db.session.query(Album.artist, Album.img, Album.title).distinct(Album.artist).order_by(desc(Album.artist)).all()
	for album in albums:
		if album[0] != None and album[0][0].lower() == firstChar.lower():
			import unicodedata
			artist = album[0].encode('ascii','ignore').decode()
			title = album[2].encode('ascii','ignore').decode()
			img = str(album[1]).encode('ascii','ignore').decode()
			return((img, title, artist))

def getArtistesByString(chaine):
	"""
	retourne les artistes correspondants a une chaine donnee
	"""
	from sqlalchemy import desc
	a = db.session.query(Album.artist).distinct(Album.artist).order_by(desc(Album.artist)).all()
	res=[]
	l = len(chaine)
	for artiste in a:
		if artiste[0] != None and artiste[0][0:l].lower() == chaine.lower():
			res.append( transcode(artiste[0]) )
	return sorted(res)

def getGenreArtist(artist):
	"""
	return all genres find for an artist
	"""
	from sqlalchemy import desc
	albums_ID = db.session.query(Album.Id).filter((Album.artist) == artist).all()
	albums_ID =  [x[0] for x in albums_ID]

	genre_des_albums = []
	for albumID in albums_ID:
		genre_des_albums.extend(getGenreAlbum(albumID))
	return sorted(list(set(genre_des_albums)))

def getGenreAlbum(albumID):
	"""
	return les genres de l'id de l'album donne
	"""
	return [x[0] for x in db.session.query(Genre.genre).filter(Genre.entryId == albumID).all()]

def getAlbums(artist):
	"""
	retourne les albums(titre, annee) d'un artiste
	"""
	from sqlalchemy import desc
	albums = db.session.query(Album.title, Album.releaseYear, Album.img).filter((Album.artist) == artist).order_by(desc(Album.releaseYear)).all()
	return albums

def recherche(recherche, _type, page_size=10, page=0):
	"""
	systeme de recherche
	"""
	if _type==0:
		return rechercheTitre(recherche, page_size, page)
	if _type==1:
		return rechercheArtiste(recherche, page_size, page)
	if _type==2:
		return rechercheGenre(recherche, page_size, page)
	if _type==3:
		return rechercheAnnee(recherche, page_size, page)

def rechercheTitre(recherche, page_size, page):
	from sqlalchemy import func
	return db.session.query(Album.title, Album.img, Album.releaseYear, Album.artist).filter(func.lower(Album.title).contains(recherche.lower())).limit(page_size).offset(page*page_size).all()

def rechercheArtiste(recherche, page_size, page):
	from sqlalchemy import func
	return db.session.query(Album.title, Album.img, Album.releaseYear, Album.artist).filter(func.lower(Album.artist).contains(recherche.lower())).limit(page_size).offset(page*page_size).all()


def rechercheAnnee(recherche, page_size, page):
	from sqlalchemy import func
	return db.session.query(Album.title, Album.img, Album.releaseYear, Album.artist).filter(Album.releaseYear == recherche).limit(page_size).offset(page*page_size).all()


def rechercheGenre(recherche, page_size, page):
	from sqlalchemy import func
	albums_ID = db.session.query(Genre.entryId).filter(func.lower(Genre.genre).contains(recherche.lower()))
	albums_ID = [x[0] for x in albums_ID]
	
	res=[]
	for ID in albums_ID[page*page_size:(page+1)*page_size]:
		res.append(getAlbumWithID(ID))
	return res

def getAlbumWithID(ID):
	"""
	retourne l'album correspondant a l'id donne
	"""
	return db.session.query(Album.title, Album.img, Album.releaseYear, Album.artist).filter(Album.Id == ID).first()