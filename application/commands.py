from .app import manager,db
from .models import transcode
import sys, codecs

@manager.command
def loaddb(filename):

	"""Create the tables and populates them with data"""
	print("import du YAML")
	db.create_all()
	nb_alb = 0
	
	import yaml
	print("lecture du fichier")
	source = yaml.load(codecs.open(filename, 'r', encoding='utf8', errors="ignore"))
	print("fichier lu")

	from .models import Album, Genre

	for album in source:
		a = Album(Id = album["entryId"], artist = album["by"], img = album["img"], parent = album["parent"], title = album["title"], releaseYear = album["releaseYear"])
		g = album["genre"]
		
		for genre in g:
			db.session.add(Genre(entryId = int(album["entryId"]),genre = transcode(genre) ))
		db.session.add(a);
		nb_alb += 1
		#print("album %i ajoute") % (nb_alb)

	db.create_all()
	db.session.commit()
	print("import fini")